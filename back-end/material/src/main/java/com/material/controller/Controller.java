package com.material.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.material.model.StaffData;
import com.material.service.StaffService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("blade/school")
public class Controller {
	
	@Autowired
	private StaffService staffService;
	
	@GetMapping("/getStafflist")
	public Iterable<StaffData> getAllStaff(){
		return this.staffService.get();
	}
	
	@PostMapping("/addStaff")
	public StaffData addStaff(@RequestBody StaffData data) {
		return this.staffService.post(data);
	}
	
	@PutMapping("/updateStaff/{id}")
	public StaffData editStaff(@PathVariable (value="id")Integer id,
			@RequestBody StaffData data) {
		return this.staffService.edit(id, data);
	}
	
	@DeleteMapping("/deleteStaff/{id}")
	public void DeleteStaff(@PathVariable (value="id") Integer id) {
		this.staffService.delete(id);
	}

}
