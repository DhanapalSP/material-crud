package com.material.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.material.model.StaffData;
import com.material.repositary.StaffRepository;

@Service
public class StaffService {	
	
	@Autowired
	private StaffRepository staffRep;
	
	public Iterable<StaffData> get(){
		
		return this.staffRep.findAll();
	}
	
	public StaffData post(StaffData Staff) {
		
		return this.staffRep.save(Staff);
	}
	
	public StaffData edit(Integer id, StaffData data) {
		
		StaffData staff = this.staffRep.findById(id)
				.orElseThrow(()->new ResourceNotFoundException("Note:Id is invalid.... Can't update "));
		staff.setName(data.getName());
		staff.setAge(data.getAge());
		staff.setContact(data.getContact());
		staff.setDegree(data.getDegree());
		staff.setDegreeBackground(data.getDegreeBackground());
		staff.setDob(data.getDob());
		staff.setExperience(data.getExperience());
		staff.setSalary(data.getSalary());
		
		return this.staffRep.save(staff);
	}
	
	
	public void delete(Integer id) {
		
		 this.staffRep.deleteById(id);
	}

}
