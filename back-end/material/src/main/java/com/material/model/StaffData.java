package com.material.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StaffData {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private int age;
	private String dob;
	private String degreeBackground;
	private String degree;
	private String experience;
	private int contact;
	private int salary;
	
	
	
	public StaffData(int id, String name, int age, String dob, String degreeBackground, String degree,
			String experience, int contact, int salary) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.dob = dob;
		this.degreeBackground = degreeBackground;
		this.degree = degree;
		this.experience = experience;
		this.contact = contact;
		this.salary = salary;
	}
	
	
	
	public StaffData() {
		
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getDegreeBackground() {
		return degreeBackground;
	}
	public void setDegreeBackground(String degreeBackground) {
		this.degreeBackground = degreeBackground;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public int getContact() {
		return contact;
	}
	public void setContact(int contact) {
		this.contact = contact;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
}
