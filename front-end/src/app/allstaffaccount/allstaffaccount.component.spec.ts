import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllstaffaccountComponent } from './allstaffaccount.component';

describe('AllstaffaccountComponent', () => {
  let component: AllstaffaccountComponent;
  let fixture: ComponentFixture<AllstaffaccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllstaffaccountComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllstaffaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
