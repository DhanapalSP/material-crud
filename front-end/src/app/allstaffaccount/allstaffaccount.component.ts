// import { Component, OnInit } from '@angular/core';
import { SchoolService } from '../school.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-allstaffaccount',
  templateUrl: './allstaffaccount.component.html',
  styleUrls: ['./allstaffaccount.component.css'],
})
export class AllstaffaccountComponent implements OnInit {
  title = 'StaffList';
  displayedColumns: string[] = [
    'name',
    'age',
    'dob',
    'degreeBackground',
    'degree',
    'experience',
    'contact',
    'salary',
    'action',
  ];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  staffdetails!: FormGroup;
  constructor(private api: SchoolService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.getStaff();
  }

  getStaff() {
    this.api.getStafflist().subscribe({
      next: (data) => {
        // console.log(data);
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error: (res) => {
        alert('Error while retrieving stafflist');
      },
    });
  }
  editStaff(row: any) {
    this.dialog
      .open(DialogComponent, {
        width: '30%',
        data: row,
      })
      .afterClosed()
      .subscribe((val) => {
        if (val === 'update') {
          this.getStaff();
        }
      });
  }
  deleteStaff(id: number) {
    this.api.deleteStaff(id).subscribe({
      next: (data) => {
        alert('Deleted Succesfully..!');
        this.getStaff();
      },
      error: (err) => {
        alert('Unable to delete');
      },
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
