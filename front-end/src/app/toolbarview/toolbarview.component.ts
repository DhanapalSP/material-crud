import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AllstaffaccountComponent } from '../allstaffaccount/allstaffaccount.component';
import { DialogComponent } from '../dialog/dialog.component';
import { SchoolService } from '../school.service';

@Component({
  selector: 'app-toolbarview',
  templateUrl: './toolbarview.component.html',
  styleUrls: ['./toolbarview.component.css'],
})
export class ToolbarviewComponent {
  // @ViewChild(AllstaffaccountComponent)
  // aslist!: AllstaffaccountComponent;
  constructor(private dialog: MatDialog, private service: SchoolService) {}

  ngOnInit(): void {}

  openDialog() {
    this.dialog.open(DialogComponent, {
      width: '30%',
    });
    // .afterClosed()
    // .subscribe((val) => {
    //   if (val === 'save') {
    //     this.aslist.getStaff();
    //   }
    // });
    // demo 
  }
}
