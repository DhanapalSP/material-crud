import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarviewComponent } from './toolbarview.component';

describe('ToolbarviewComponent', () => {
  let component: ToolbarviewComponent;
  let fixture: ComponentFixture<ToolbarviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToolbarviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ToolbarviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
