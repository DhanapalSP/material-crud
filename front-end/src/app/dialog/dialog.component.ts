import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { SchoolService } from '../school.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
})
export class DialogComponent {
  // @ViewChild(AllstaffaccountComponent, { static: true })
  // aslist!: AllstaffaccountComponent;

  action: String = 'Save';
  heading: String = 'Add Staff Details';
  years = [
    'Fresher ',
    '0-1 year',
    '1-3 years',
    '3-5 years',
    'More than 5 years ',
  ];
  staffdetails!: FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: SchoolService,
    @Inject(MAT_DIALOG_DATA) public editData: any,
    private dialogref: MatDialogRef<DialogComponent>
  ) {}

  ngOnInit(): void {
    this.staffdetails = this.builder.group({
      id: [''],
      name: ['', Validators.required],
      age: ['', Validators.required],
      dob: ['', Validators.required],
      degreeBackground: ['', Validators.required],
      degree: ['', Validators.required],
      experience: ['', Validators.required],
      contact: ['', Validators.required],
      salary: ['', Validators.required],
    });

    if (this.editData) {
      this.action = 'Update';
      this.heading = 'Update staff Details';
      this.staffdetails.controls['name'].setValue(this.editData.name);
      this.staffdetails.controls['age'].setValue(this.editData.age);
      this.staffdetails.controls['dob'].setValue(this.editData.dob);
      this.staffdetails.controls['degreeBackground'].setValue(
        this.editData.degreeBackground
      );
      this.staffdetails.controls['degree'].setValue(this.editData.degree);
      this.staffdetails.controls['experience'].setValue(
        this.editData.experience
      );
      this.staffdetails.controls['contact'].setValue(this.editData.contact);
      this.staffdetails.controls['salary'].setValue(this.editData.salary);
    }
  }

  // getStaff() {
  //   this.service.getStafflist().subscribe((data) => {
  //     console.log(data);
  //   });
  // }

  addstaff() {
    console.log(this.staffdetails.value);
    if (!this.editData) {
      if (this.staffdetails.valid) {
        this.service.addStaff(this.staffdetails.value).subscribe({
          next: (res) => {
            alert('New staff added successfully....!');
            window.location.reload();
            this.staffdetails.reset();
            this.dialogref.close('save');

            // this.getStaff();
            // this.aslist.getStaff();
          },
          error: (res) => {
            alert('Error while adding the new-staff');
          },
        });
      }
    } else {
      this.updateStaff();
    }
  }

  updateStaff() {
    if (this.staffdetails.valid) {
      console.log(this.editData.id);
      this.service
        .editStaff(this.editData.id, this.staffdetails.value)
        .subscribe({
          next: (res) => {
            alert('Update Successfully...!');
            this.staffdetails.reset();
            this.dialogref.close('update');
          },
          error: (res) => {
            alert('Error while Updating');
          },
        });
    } else {
      alert('form is not valid...!');
    }
  }
}
