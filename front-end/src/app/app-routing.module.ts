import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllstaffaccountComponent } from './allstaffaccount/allstaffaccount.component';
import { AppComponent } from './app.component';
import { Demo1Component } from './demo1/demo1.component';

const routes: Routes = [
  { path: 'demo1', component: Demo1Component },
  {
    path: 'demo2',
    loadChildren: () =>
      import('./demo2/dem2/dem2.module').then((d2) => d2.Dem2Module),
    // import('./demo2/demo2.component').then((dem1) => dem1.Demo2Component),
  },
  {
    path: 'stafflist',
    component: AppComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
