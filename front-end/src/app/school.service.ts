import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const url = 'http://localhost:8080/blade/school/';
@Injectable({
  providedIn: 'root',
})
export class SchoolService {
  constructor(private http: HttpClient) {}

  getStafflist(): Observable<any> {
    return this.http.get(url + 'getStafflist');
  }

  addStaff(data: Object): Observable<Object> {
    return this.http.post(url + 'addStaff', data);
  }

  editStaff(id: number, data: Object): Observable<Object> {
    return this.http.put(url + 'updateStaff/' + id, data);
  }

  deleteStaff(id: number): Observable<Object> {
    return this.http.delete(url + 'deleteStaff/' + id);
  }
}
