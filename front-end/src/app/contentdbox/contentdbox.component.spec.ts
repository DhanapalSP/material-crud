import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentdboxComponent } from './contentdbox.component';

describe('ContentdboxComponent', () => {
  let component: ContentdboxComponent;
  let fixture: ComponentFixture<ContentdboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentdboxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContentdboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
