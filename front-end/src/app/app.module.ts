import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { MatButton, MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { Demo1Module } from './demo1/demo1/demo1.module';
import { MatDialogModule } from '@angular/material/dialog';
import {
  ContentdboxComponent,
  DialogContentExampleDialog,
} from './contentdbox/contentdbox.component';
import { StafflistComponent } from './stafflist/stafflist.component';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ToolbarviewComponent } from './toolbarview/toolbarview.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { DialogComponent } from './dialog/dialog.component';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { AllstaffaccountComponent } from './allstaffaccount/allstaffaccount.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentdboxComponent,
    DialogContentExampleDialog,
    StafflistComponent,
    ToolbarviewComponent,
    DialogComponent,
    AllstaffaccountComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    MatIconModule,
    Demo1Module,
    MatDialogModule,
    MatTableModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatRadioModule,
  ],

  providers: [],
  bootstrap: [AppComponent],
  exports: [MatButton],
})
export class AppModule {}
