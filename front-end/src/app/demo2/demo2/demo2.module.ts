import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Demo2Component } from '../demo2.component';

@NgModule({
  declarations: [Demo2Component],
  imports: [CommonModule],
})
export class Demo2Module {
  constructor() {
    console.log('demo2');
  }
}
