import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Dem2RoutingModule } from './dem2-routing.module';
import { Demo2Component } from '../demo2.component';

@NgModule({
  declarations: [Demo2Component],
  imports: [CommonModule, Dem2RoutingModule],
})
export class Dem2Module {
  constructor() {
    console.log('demo2');
  }
}
